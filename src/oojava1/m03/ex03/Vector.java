package oojava1.m03.ex03;

public class Vector {
	private double x, y;
	
	public Vector(double x, double y) {
		this.x=x;
		this.y=y;
	}
	
	public static Vector add(Vector a, Vector b) {
		return new Vector(a.getX()+b.getX(), a.getY()+b.getY());
	}
	
	public static double addX(Vector a, Vector b) {
		return a.getX()+b.getX();
	}
	public static double addY(Vector a, Vector b) {
		return a.getY()+b.getY();
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public String toString() {
		return String.format("(%f;%f)",x,y);
	}	

}
